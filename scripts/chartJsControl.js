var app = angular.module('myApp', []);
var dataPoints = [];
var lineOneChart = [];
var lineTwoChart = [];
var lineThreeChart = [];
var lineFourChart = [];
var lineFiveChart = [];
var dataHora = [];
var chartBarCollor = [];
var borderChart = [];
var dataTable = [];
var identificadores = ["sala", "fin", "eng", "fat", "tec"];
var rgba;
var border;

//Carrega o gráfico antes dos dados
new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
        //  labels: ["Sala 08", "Financeiro", "Engenharia", "Faturamento", "Tecnologia"],
        datasets: [{
            backgroundColor: chartBarCollor,
            borderColor: borderChart,
            borderWidth: 1,
            label: "Temperatura",
            data: dataPoints
        }]
    },
    options: {
        title: {
            display: true,
            text: 'Gráfico de Temperatura Atual'
        },
        legend: {
            display: false,
            labels: {
                fontColor: 'rgb(0,0,0)'
            }
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    color: ['rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'red', 'rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'blue', 'rgba(0,0,255, 0.1)']
                },
                ticks: {
                    beginAtZero: true,
                    min: 18,
                    max: 26,
                    stepSize: 1,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Temperatura (°C)'
                }
            }]
        }
    }
});

// Carrega gráfico de linhas
new Chart(document.getElementById('line-chart'), {
    type: 'line',
    data: {
        labels: dataHora,
       datasets: [
            {
                data: lineOneChart.slice(0).reverse(),
                label: "Sala 08",
                backgroundColor: "#3e95cd",
                borderColor: "#3e95cd",
                fill: false
            }, {
                data: lineTwoChart.slice(0).reverse(),
                label: "Financeiro",
                backgroundColor: "#8e5ea2",
                borderColor: "#8e5ea2",
                fill: false
            }, {

                data: lineThreeChart.slice(0).reverse(),
                label: "Engenharia",
                backgroundColor: "#3cba9f",
                borderColor: "#3cba9f",
                fill: false
            }, {
                data: lineFourChart.slice(0).reverse(),
                label: "Faturamento",
                backgroundColor: "#e8c3b9",
                borderColor: "#e8c3b9",
                fill: false
            }, {
                data: lineFiveChart.slice(0).reverse(),
                label: "Tecnologia",
                backgroundColor: "#c45850",
                borderColor: "#c45850",
                fill: false
                }
            ]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Histórico da Temperatura'
        },
        legend: {
            display: true,
            positon: 'botoom'
        },
        scales: {
            yAxes: [{
            
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Temperatura (°C)'
                },
            }]
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
    }
});

// Popula a tabela
$.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensores", function (data) {
    $.each(data.cliente, function (key, item) {
        dataTable.push
            (
            parseFloat(item.value)
            );

    });
    for (let _j = 0; _j < dataTable.length; _j++) {
        var id = document.getElementById(identificadores[_j]);
        $("#" + identificadores[_j]).append("<td class='mdl-data-table__cell--non-numeric'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dataTable[_j] + " °C </td>");
        if (dataTable[_j] > 23) {

            $("#" + identificadores[_j]).append("<td class='mdl-data-table__cell--non-numeric'>&nbsp;Alta</td>");
        } else
            if (dataTable[_j] < 20) {
                $("#" + identificadores[_j]).append("<td class='mdl-data-table__cell--non-numeric'>&nbsp;Baixa</td>");
            } else {
                $("#" + identificadores[_j]).append("<td class='mdl-data-table__cell--non-numeric'>&nbsp;Normal</td>");
            }

    }
});

// Popula o gráfico
function updateChart() {
    $.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensores", function (data) {
        $.each(data.cliente, function (key, item) {
            dataPoints.push
                (
                parseFloat(item.value)
                );
        });

        for (let _i = 0; _i < dataPoints.length; _i++) {

            if (dataPoints[_i] > 23.0) {

                rgba = 'rgba(255,0,0, 0.4)';
                border = 'rgba(255,0,0, 0.4)';

                chartBarCollor.push(rgba);
                borderChart.push(border);

            } else
                if (dataPoints[_i] < 20.0) {

                    rgba = 'rgba(255,0,0, 0.1)';
                    border = 'rgba(255,0,0, 0.4)';

                    chartBarCollor.push(rgba);
                    borderChart.push(border);
                }
                else
                    if (dataPoints[_i] >= 20.0 && dataPoints[_i] <= 23.0) {

                        rgba = 'rgba(255,0,0, 0.2)';
                        border = 'rgba(255,0,0, 0.4)';

                        chartBarCollor.push(rgba);
                        borderChart.push(border);
                    }
        }
        new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
                labels: ["Sala 08", "Financeiro", "Engenharia", "Faturamento", "Tecnologia"],
                datasets: [{
                    backgroundColor: chartBarCollor,
                    borderColor: borderChart,
                    borderWidth: 1,
                    label: "Temperatura",
                    data: dataPoints
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Gráfico de Temperatura Atual'
                },
                legend: {
                    display: false,
                    labels: {
                        fontColor: 'rgb(0,0,0)'
                    }
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            drawBorder: false,
                            color: ['rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'red', 'rgba(0,0,255, 0.1)', 'rgba(0,0,255, 0.1)', 'blue', 'rgba(0,0,255, 0.1)']
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 18,
                            max: 26,
                            stepSize: 1,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Temperatura (°C)'
                        }
                    }]
                }
            }
        });
    });
}

// Popula o Gráfico de linha
function updateLineChart() {
    $.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensor1", function (data) {
        $.each(data.cliente, function (key, item) {
            lineOneChart.push
                (
                parseFloat(item.value)
                )
            dataHora.push
                (
                item.data.substring(11, 19)
                )
        });

        $.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensor2", function (data) {
            $.each(data.cliente, function (key, item) {
                lineTwoChart.push
                    (
                    parseFloat(item.value)
                    )
            });
            $.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensor3", function (data) {
                $.each(data.cliente, function (key, item) {
                    lineThreeChart.push
                        (
                        parseFloat(item.value)
                        )
                });

                $.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensor4", function (data) {
                    $.each(data.cliente, function (key, item) {
                        lineFourChart.push
                            (
                            parseFloat(item.value)
                            )
                    });
                    $.getJSON("http://10.40.0.237:8080/Zatix/temperatura/listarSensor5", function (data) {
                        $.each(data.cliente, function (key, item) {
                            lineFiveChart.push
                                (
                                parseFloat(item.value)
                                )
                        });
                        new Chart(document.getElementById('line-chart'), {
                            type: 'line',
                            data: {
                                labels: dataHora.slice(0).reverse(),
                                datasets: [
                                    {
                                        data: lineOneChart.slice(0).reverse(),
                                        label: "Sala 08",
                                        backgroundColor: "#3e95cd",
                                        borderColor: "#3e95cd",
                                        fill: false
                                    }, {
                                        data: lineTwoChart.slice(0).reverse(),
                                        label: "Financeiro",
                                        backgroundColor: "#8e5ea2",
                                        borderColor: "#8e5ea2",
                                        fill: false
                                    }, {

                                        data: lineThreeChart.slice(0).reverse(),
                                        label: "Engenharia",
                                        backgroundColor: "#3cba9f",
                                        borderColor: "#3cba9f",
                                        fill: false
                                    }, {
                                        data: lineFourChart.slice(0).reverse(),
                                        label: "Faturamento",
                                        backgroundColor: "#e8c3b9",
                                        borderColor: "#e8c3b9",
                                        fill: false
                                    }, {
                                        data: lineFiveChart.slice(0).reverse(),
                                        label: "Tecnologia",
                                        backgroundColor: "#c45850",
                                        borderColor: "#c45850",
                                        fill: false
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                title: {
                                    display: true,
                                    text: 'Histórico de Temperatura'
                                },
                                legend: {
                                    display: true,
                                    positon: 'botoom'
                                },
                                scales: {
                                    yAxes: [{

                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Temperatura (°C)'
                                        },
                                    }]
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                }
                            }
                        });
                    });
                });
            });
        });
    });
}

// Atualiza a data do update
app.controller('myCtrl', function ($scope) {

    $scope.today = new Date();
    $scope.date = $scope.today.toLocaleDateString();
    $scope.hour = $scope.today.toLocaleTimeString();

    $scope.dateNow = ($scope.date + ' ' + $scope.hour);

});

// Atualizada o dados dos gráficos e da tabela
setTimeout(function () {

    updateChart();
    updateLineChart();

    setInterval(function () {
        location.reload();

    }, 600000);
});